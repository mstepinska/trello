require('dotenv').config()
const express     = require('express')
const bodyParser  = require('body-parser')
const pino        = require('express-pino-logger')()
const fallback    = require('express-history-api-fallback')
const moment      = require('moment')

const { get_as_map } = require('./utils/utils.js')
const { get_bucket_files, save_file_to_bucket, remove_file_from_bucket, read_file_from_bucket} = require('./utils/aws_utils.js')
const { fetch_board_cards, fetch_board_info, get_board_full_export, add_label_to_cards_on_list, create_board, create_board_labels,
  create_board_lists, create_cards, get_card_details } = require( './utils/trello_api.js')

const app = express()

const BOARDS = [
  { 'id': 'current', 'name': 'Current', 'trello_id': process.env.TRELLO_BOARD_SPRINT_ID  },
  { 'id': 'backlog', 'name': 'Backlog', 'trello_id': process.env.TRELLO_BOARD_BACKLOG_ID },
  { 'id': 'custom',  'name': 'Custom',  'trello_id': process.env.TRELLO_BOARD_PRIVATE_ID }
]

app.use( bodyParser.json() )
app.use(bodyParser.urlencoded({ extended: false }))
app.use(pino)

app.get('/api/boards', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  res.json(BOARDS)
})

app.get('/api/backup_files', (req, res) => {
  get_bucket_files(res)
})

app.post('/api/backup_file/read', (req, res) => {
  const { body } = req
  const { filename } = body

  read_file_from_bucket(filename, res, (e) => {throw e})
})

app.post('/api/backup_file/remove', (req, res) => {
  const { body } = req
  const { filename } = body

  remove_file_from_bucket(filename)
  res.status(200).send('Removed')
})

app.post('/api/board/create', (req, res) => {
  const { body } = req
  const { board } = body

  create_board(board)
    .then(response => {
      const { data } = response
      const { id, shortUrl } = data

      res.json({id, shortUrl})
    })
    .catch(e => {
      throw e
    })
})

app.post('/api/board/:id/create/labels', (req, res) => {
  const id = req.params.id

  const { body } = req
  const { labels } = body

  create_board_labels(id, labels)
    .then(response =>
      res.json(response)
    )
    .catch(e => {
      throw e
    })
})

app.post('/api/board/:id/create/lists', (req, res) => {
  const id = req.params.id

  const { body } = req
  const { lists } = body

  create_board_lists(id, lists)
    .then(response =>
      res.json(response)
    )
    .catch(e => {
      throw e
    })
})

app.get('/api/board/:id/cards', (req, res) => {
  const id = req.params.id
  
  fetch_board_cards(id)
    .then(response => {
      res.json(response)
    })
    .catch(e => {
      throw e
    })
})

app.get('/api/board/:id/info', (req, res) => {
  const id = req.params.id

  fetch_board_info(id)
    .then(response => {
      res.json(response)
    })
    .catch(e => {
      throw e
    })
})

app.get('/api/card/:id/info', (req, res) => {
  const id = req.params.id

  get_card_details(id)
    .then(response => {
      res.json(response)
    })
    .catch(e => {
      throw e
    })
})

app.get('/api/board/:id/backup', (req, res) => {
  const id = req.params.id

  const boards_to_backup = (id === 'all') ? BOARDS : [get_as_map(BOARDS, 'trello_id')[id]]

  Promise.all(boards_to_backup.map(board => (get_board_full_export(board.trello_id))))
    .then(board_responses => {

      boards_to_backup.forEach((board, i) => {
        const filename = `${board.name}_${board.trello_id}_${moment().format('YYYYMMDD')}.json`
        save_file_to_bucket(filename, board_responses[i].data)
      })

      res.status(200).send('Created')
    })
    .catch(e => {
      throw e
    })
})

app.get('/api/list/:list_id/add/label/:label_id', (req, res) => {
  const list_id = req.params.list_id
  const label_id = req.params.label_id

  add_label_to_cards_on_list(list_id, label_id)
    .then(response => {
      res.json(response)
    })
    .catch(e => {
      throw e
    })
})

app.post('/api/cards/create', (req, res) => {
  const { body } = req
  const { cards } = body
  create_cards(cards)
    .then(response => {
      res.json(response)
    })
    .catch(e => {
      throw e
    })
})

// For all other urls, serve Client from disk
app.use('/', express.static('public'))
app.use(fallback('index.html', { root: `${__dirname}/public` }))

app.listen(5151, () =>
  console.log('Express server is running on localhost:5151')
)