const aws     = require('aws-sdk')

const AWS_ACCESS_KEY_ID     = process.env['AWS_ACCESS_KEY_ID'] || undefined
const AWS_SECRET_ACCESS_KEY = process.env['AWS_SECRET_ACCESS_KEY'] || undefined
const AWS_REGION            = process.env['AWS_REGION'] || undefined

const S3_TRELLO_BUCKET      = process.env['S3_TRELLO_BUCKET']  || 'aistemos-trello'


function s3_conn() {
  if (AWS_ACCESS_KEY_ID && AWS_SECRET_ACCESS_KEY && AWS_REGION) {

    aws.config.update(
      {
        'accessKeyId': AWS_ACCESS_KEY_ID,
        'secretAccessKey': AWS_SECRET_ACCESS_KEY,
        "region": AWS_REGION
      }
    )
  }

  return new aws.S3()
}

function get_bucket_files(res) {
  const s3 = s3_conn()
  const params = {
    Bucket: S3_TRELLO_BUCKET,
    Delimiter: '',
    Prefix: ''
  }

  s3.listObjects(params, function (err, data) {
    if(err)throw err
    const {Contents} = data
    res.json(Contents.map(file => (file.Key)))
  })
}

function read_file_from_bucket(file_name, res, next) {
  const s3 = s3_conn()
  const params = { Bucket: S3_TRELLO_BUCKET, Key: file_name }
  s3.getObject(params, function(err, data) {
    if (err) {
      next(err)
    } else {
      try {
        res.json(JSON.parse(data.Body))
      }
      catch(err) {
        // This can happen if JSON is invalid or incomplete (i.e. when updating files)
        next(err)
      }
    }
  })
}
function remove_file_from_bucket(file_name) {
  const s3 = s3_conn()
  const params = { Bucket: S3_TRELLO_BUCKET, Delete: {
      Objects: [
        {Key: file_name}
      ]
    }
  }
  s3.deleteObjects(params, function(err, data) {
    if (err) {
      throw err
    }
    else {
      console.log('delete', data)
    }
  })
}

function save_file_to_bucket(file_name, data) {
  const s3 = s3_conn()
  const params = {
    Bucket: S3_TRELLO_BUCKET,
    Key: file_name,
    Body: JSON.stringify(data, null, 2)
  }

  return s3.upload(params, function(err, data) {
    if (err) {
      return err
    } else {
      return 'ok'
    }
  })
}

module.exports.read_file_from_bucket = read_file_from_bucket
module.exports.save_file_to_bucket = save_file_to_bucket
module.exports.get_bucket_files = get_bucket_files
module.exports.remove_file_from_bucket = remove_file_from_bucket