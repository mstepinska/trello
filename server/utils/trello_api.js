const axios = require('axios')

const { get_as_map } = require('./utils.js')

const TRELLO_API_URL = 'https://api.trello.com/1'
const CARD_FIELDS = 'name,labels,desc,shortUrl,dateLastActivity,idMembers,idList,closed'

function get_board_url(board_id) {
  return `${TRELLO_API_URL}/boards/${board_id}`
}

function get_list_url(list_id) {
  return `${TRELLO_API_URL}/lists/${list_id}`
}

function get_cards_url(card_id) {
  return `${TRELLO_API_URL}/cards/${card_id}`
}

function get_card_url(card_id) {
  return `${TRELLO_API_URL}/card/${card_id}`
}

function get_credentials() {
  const key = process.env.TRELLO_KEY
  const token = process.env.TRELLO_TOKEN

  return `key=${key}&token=${token}`
}

function fetch_board_lists(board_id) {
  const url = get_board_url(board_id) + `/lists/?${get_credentials()}`
  return axios.get(url)
}

function fetch_board_members(board_id) {
  const url = get_board_url(board_id) + `/members/?${get_credentials()}`

  return axios.get(url)
}

function fetch_board_labels(board_id) {
  const url = get_board_url(board_id) + `/labels/?${get_credentials()}`
  return axios.get(url)
}

function fetch_board_info(board_id) {
  return axios.all([fetch_board_lists(board_id), fetch_board_members(board_id), fetch_board_labels(board_id)])
    .then(([lists_response, members_response, labels_response]) => {
      return {
        'lists': lists_response.data,
        'members': members_response.data,
        'labels': labels_response.data
      }
    })
}

function fetch_board_cards(board_id) {
  return fetch_board_info(board_id)
    .then(board_info => {
      const url = get_board_url(board_id) + `/cards/?${get_credentials()}&fields=${CARD_FIELDS}`
      return axios.get(url).then(response => {

        const board_lists = get_as_map(board_info.lists, 'id')
        const board_members = get_as_map(board_info.members, 'id')

        const cards = response.data

        const processed_data = cards.map(card => (
          {...card, list: board_lists[card.idList].name, members: card.idMembers.map(member => (board_members[member].fullName))}
        ))

        return {
          ...board_info,
          cards: processed_data
        }
      })
  })
}

function fetch_card_info(card_id) {
  const url = `${get_cards_url(card_id)}?${get_credentials()}&fields=all`
  return axios.get(url)
}

function fetch_card_actions(card_id) {
  const url = `${get_cards_url(card_id)}/actions?${get_credentials()}`
  return axios.get(url)
}

function get_card_details(card_id) {
  return axios.all([fetch_card_info(card_id), fetch_card_actions(card_id)])
    .then(([info_response, actions_response]) => ({info: info_response.data, actions: actions_response.data}))
}

function fetch_list_cards(list_id) {
  const url = `${get_list_url(list_id)}/cards?${get_credentials()}`
  return axios.get(url)
}

function post(url) {
  return axios.post(url)
    .catch(e => {
      console.log(e.message)
    })
}

function add_card(card) {
  const url = `${TRELLO_API_URL}/cards?${get_credentials()}&idList=${card.idList}&name=${encodeURIComponent(card.name)}&desc=${encodeURIComponent(card.desc)}&idLabels=${card.idLabels.join(',')}`
  return axios.post(url)
    .catch(e => {
      console.log(e.message + ' ' + card.name)
    })
}

function add_label_to_cards_on_list(list_id, label_id) {
  return fetch_list_cards(list_id)
    .then(response => {

      const cards = response.data

      const promises = cards.map(card => {
        const url = `${get_card_url(card.id)}/idLabels?${get_credentials()}&value=${label_id}`
        return post(url)
      })

      return axios.all(promises)
        .then(response => {
          return response.map(item => (item.data))
        })
    })
}

function get_board_full_export(board_id) {
  if (!board_id) return

  const all = {
    fields: 'all',
    actions: 'all',
    action_fields: 'all',
    actions_limit: 1000,
    cards: 'all',
    card_fields: 'all',
    card_attachments: true,
    labels: 'all',
    lists: 'all',
    list_fields: 'all',
    members: 'all',
    member_fields: 'all',
    checklists: 'all',
    checklist_fields: 'all',
    organization: false
  }
  
  const params = Object.keys(all).map(key => (`${key}=${all[key]}`))
  
  const url = get_board_url(board_id) + `?${get_credentials()}&${params.join('&')}`

  return axios.get(url)
}

function create_board(board_params) {
  if (!board_params) return null

  const params = Object.keys(board_params).map(param => (`${param}=${encodeURIComponent(board_params[param])}`))

  const url = 'https://api.trello.com/1/boards?' + get_credentials() + '&' + params.join('&')
  return axios({
    method: 'POST',
    url: url
  })
}

function create_board_labels(board_id, labels) {
  const promises = labels.map(label => {
    return post(get_board_url(board_id) + `/labels?${get_credentials()}&name=${encodeURIComponent(label.name)}&color=${label.color}`)
  })

  return axios.all(promises)
    .then(response => {
      return response.map((item, i) => ({...item.data, old_id: labels[i].id}))
    })
}

function create_board_lists(board_id, lists) {
  const promises = lists.map(list => {
    return post(get_board_url(board_id) + `/lists?${get_credentials()}&name=${encodeURIComponent(list.name)}&pos=${list.pos}`)
  })

  return axios.all(promises)
    .then(response => {
      return response.map((item, i) => ({...item.data, old_id: lists[i].id}))
    })
}

function create_cards(cards) {
  const promises = cards.map(card => {
    return add_card(card)
  })

  return axios.all(promises)
    .then(response => {
      return response.map(item => {
        const {data} = item || {}
        return data
      })
    })
}

module.exports = {
  fetch_board_cards,
  get_board_full_export,
  fetch_board_info,
  add_label_to_cards_on_list,
  create_board,
  create_board_labels,
  create_board_lists,
  create_cards,
  get_card_details
}