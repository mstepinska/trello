import axios from 'axios'

export const COLOURS = ['yellow', 'purple', 'blue', 'green', 'orange', 'sky', 'pink', 'lime']

export function fetch_board_info(board_id) {
  return axios.get('/api/board/'+board_id+'/info')
    .then(response => (response.data))
}

export function fetch_board_cards(board_id) {
  return axios.get('/api/board/'+board_id+'/cards')
    .then(response => (response.data))
}

export function add_label_to_cards_on_list(list_id, label_id) {
  return axios({
    method: 'GET',
    url: '/api/list/'+list_id+'/add/label/'+label_id,
  }).then(response => (response.data))
}

export function board_export(board_id) {
  return axios.get(`/api/board/${board_id}/backup`)
    .then(response => response)
    .catch(e => {
      throw e
    })
}

export function get_available_boards() {
  return axios.get('/api/boards')
  .then(response => (response.data))
}

export function fetch_backup_files() {
  return axios.get('/api/backup_files')
    .then(response => (response.data))
}

export function fetch_board_info_from_backup_file(filename) {
  return axios({
    method: 'POST',
    url: '/api/backup_file/read',
    data: {filename: filename}
  }).then(response => (response.data))
}

export function create_board(params) {
  return axios({
    method: 'POST',
    url: '/api/board/create',
    data: {board: params}
  }).then(response => {
    return response.data
  })
}

export function add_board_prerequisites({board_id, labels, lists}) {
  return Promise.all([add_board_labels(board_id, labels), add_board_lists(board_id, lists)])
    .then(([new_labels, new_lists]) => {
      return {labels: new_labels, lists: new_lists}
  })
}

export function add_board_labels(board_id, labels) {
  return axios({
    method: 'POST',
    url: `/api/board/${board_id}/create/labels`,
    data: {labels: labels}
  }).then(response => {
    return response.data
  })
}

export function add_board_lists(board_id, lists) {
  return axios({
    method: 'POST',
    url: `/api/board/${board_id}/create/lists`,
    data: {lists: lists}
  }).then(response => {
    return response.data
  })
}

export function add_board_cards(cards) {
  return axios({
    method: 'POST',
    url: '/api/cards/create',
    data: {cards: cards}
  }).then(response => {
    return response.data
  })
}

export function remove_backup_file(filename) {
  return axios({
    method: 'POST',
    url: '/api/backup_file/remove',
    data: {filename: filename}
  }).then(response => (response.data))
}

export function fetch_card_info(card_id) {
  return axios.get('/api/card/'+card_id+'/info')
    .then(response => (response.data))
}