export function get_as_map(items, key) {
  // Given an array of items, return a map to those items (using the specified key).
  return items.reduce(function(acc, item) {
    acc[item[key]] = item;
    return acc
  }, {})
}

export function sleep(delay) {
  const start = new Date().getTime()
  while (new Date().getTime() < start + delay);
}