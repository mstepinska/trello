import React, { useState, useEffect } from 'react'
import { Button, Table } from 'reactstrap'

import {
  board_export,
  create_board,
  fetch_backup_files,
  fetch_board_info_from_backup_file,
  add_board_prerequisites,
  add_board_cards, remove_backup_file
} from '../utils/boards_and_cards.js'
import { get_as_map, sleep } from '../utils/utils.js'

const Backup = ({selected_board}) => {
  const [backup_files, setBackupFiles] = useState([])
  const [should_refresh_files, setShouldRefreshFiles] = useState(true)

  useEffect(() => {
    if (should_refresh_files) {
      get_backup_files()
      setShouldRefreshFiles(false)
    }
  }, [should_refresh_files])

  function backup(board) {
    if (!board) return
    board_export(board.trello_id)
      .then(() => {
        setShouldRefreshFiles(true)
      })
      .catch(e => console.log(e))
  }

  function delete_file(backup_file) {
    remove_backup_file(backup_file)
      .then(() => {
        setShouldRefreshFiles(true)
      })
      .catch(e => console.log(e))
  }

  function get_backup_files() {
    fetch_backup_files()
      .then(response => {
        setBackupFiles(response)
      })
      .catch(e => console.log(e))
  }

  function restore_from_file(backup_file) {
    fetch_board_info_from_backup_file(backup_file)
      .then(response => {
        const { name, labels, lists, cards } = response

        create_board({name: name + '_backup', defaultLabels: false, defaultLists: false})
          .then(response => {
            const { id } = response

            const active_lists = lists.filter(list => (!list.closed))

            add_board_prerequisites({board_id: id, labels, lists: active_lists})
              .then(response => {
                const {labels, lists} = response


                const labels_by_old_id = get_as_map(labels, 'old_id')
                const lists_by_old_id = get_as_map(lists, 'old_id')

                const active_cards = cards.filter(card => (!card.closed && lists_by_old_id[card.idList]))

                const active_cards_updated = active_cards.map(card => {
                  const {name, idList, pos, idLabels, desc} = card

                  return {
                    name, pos, desc,
                    idList: (lists_by_old_id[idList] || {}).id || null,
                    idLabels: idLabels.map(label_id => (labels_by_old_id[label_id].id))
                  }
                })

                let promise = Promise.resolve(true)

                let i = 0
                let is_more = true

                while(is_more) {
                  const chunk = active_cards_updated.slice(i, i + 10)

                  i = i+10

                  is_more = i < active_cards_updated.length

                  promise = promise.then(() => {
                    return add_board_cards(chunk)
                  }).then(() => {sleep(20000)})
                }
              })
          })
      })
  }

  return (
    <div>
      <h2>Backup boards</h2>
      <div className='d-flex py-3'>
        <Button color='primary' onClick={() => backup(selected_board)}>Backup selected board</Button>
      </div>

      {backup_files && backup_files.length > 0 &&
        <div>

          <h5>Available backups</h5>
          <Table>
            <thead>
              <tr>
                <th>File name</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            {backup_files.map(file => (
              <tr key={file} >
                <td>{file}</td>
                <td><Button size='sm' onClick={() => restore_from_file(file)}>Restore</Button></td>
                <td><Button size='sm' onClick={() => delete_file(file)}>Delete</Button></td>
              </tr>
            ))}
            </tbody>
          </Table>
        </div>
      }
    </div>
  )

}

export default Backup