import React from 'react'
import { Route, Redirect, Switch } from 'react-router-dom'

import Dashboard from './Dashboard.js'

const ROUTES = [
  { path: '/dashboard', title: 'Trello cards',  main: Dashboard },
]

const Routes = () => {
  return (
    <Switch>
      <Route exact path='/' component={() => (<Redirect to='/dashboard'/>)}/>
      {ROUTES.map((route, index) => (
        <Route
          key={index}
          path={route.path}
          exact={route.exact}
          component={route.main}
        />
      ))}
      <Route render={({ location }) => (<Dashboard/>) } />
    </Switch>
  )

}

export default Routes