import React, { useState, useEffect } from 'react'
import { FormGroup, Input, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import _ from 'underscore'

import { csv_to_array } from '../utils/csv_utils.js'
import { add_board_labels, fetch_board_info, add_board_cards, COLOURS } from '../utils/boards_and_cards.js'
import { get_as_map } from '../utils/utils.js'
import TrelloCard from './TrelloCard.js'

import './Import.css'

const Import = ({selected_board}) => {
  const [cards_from_file, setCards] = useState(null)
  const [labels, setLabels] = useState(null)
  const [list, setList] = useState(null)
  const [modal_open, setModalOpen] = useState(null)
  const [board_info, setBoardInfo] = useState(null)

  useEffect(() => {
    fetch_board_info(selected_board.trello_id)
      .then(board_info =>
        setBoardInfo(board_info)
      )
  }, [selected_board])

  function modal_toggle() {
    setModalOpen(!modal_open)
  }

  function list_option_change_handler(e) {
    setList(e.target.value)
  }

  function on_change_from_file_upload(e) {
    const files = e.target.files
    if (!files.length) {
      alert('No file select')
      return
    }
    const file = files[0]
    let reader = new FileReader()
    reader.onload = function(ev) {
      parse_file(ev.target.result)
    }
    reader.readAsText(file)
  }

  function parse_file(content) {
    let filtered_content =  content.split('\n').filter((line) => !line.split(',').every(item => item.trim() === ''))

    const labels_line = filtered_content.shift()

    const labels = labels_line.split(',').map(label => (label.toLowerCase().split('"').join('')))

    const cards = filtered_content.map(item => {
      let card = {}
      const item_parts = csv_to_array(item)
      labels.forEach((label, i) => {
        if (label.trim() !== '' ) {
          card = { ...card, [label]: item_parts[i] }
        }
      })

      return card
    })

    let labels_from_file = new Set()

    cards.forEach(card => {
      labels_from_file = [...labels_from_file, ...card.labels.split('|').map(item => (item.trim()))]
    })

    labels_from_file = [...new Set(labels_from_file)]

    const labels_from_board = Object.keys(get_as_map(board_info.labels, 'name'))

    const new_labels = labels_from_file.filter(label => !labels_from_board.includes(label))
    const new_labels_with_colors = new_labels.map(label => ({name: label, color: _.sample(COLOURS)}))

    add_board_labels(selected_board.trello_id, new_labels_with_colors)
      .then(response => {
        setLabels([...board_info.labels, ...response])
        setCards(cards)
      })
  }

  function get_lists_options() {
    const { lists } = board_info || {}
    if (!lists) return null

    return lists.map((list, i) => (<option key={i} value={list.id}>{list.name}</option>))
  }

  function add_cards() {
    if (!list || list === 'no_list') return

    const name_to_label = get_as_map(labels, 'name')

    const cards_to_add = cards_from_file.map(item => {
      const name = item.name || 'New card'
      const desc = item.desc || ''

      const label_ids = item.labels.split('|').map(label => (name_to_label[label.trim()].id))

      return {
        name,
        desc,
        idLabels: label_ids,
        idList: list
      }
    })

    add_board_cards(cards_to_add)
      .then(response => {
      })
  }

  const lists_options = get_lists_options()

  return (
    <div className='my-3'>
      <h1>Import cards</h1>
      <Modal isOpen={modal_open} toggle={modal_toggle} size='lg'>
        <ModalHeader>
          Cards to add
        </ModalHeader>
        <ModalBody>
          <div className='d-flex flex-wrap'>
          {cards_from_file &&
            cards_from_file.map((card, i) => (<div key={i} className='my-1 mr-1'><TrelloCard card={card}/></div>) )
          }
          </div>
        </ModalBody>
        <ModalFooter>

        </ModalFooter>
      </Modal>

      <FormGroup>
        <Input type='file' name='file' id='file_reader' onChange={on_change_from_file_upload}  />
      </FormGroup>

      {lists_options &&
        <div className='d-flex flex-nowrap mt-3'>
          <Input type='select' selected={list} onChange={list_option_change_handler} style={{'width': 'initial'}}>
            <option value={'no_list'}>[select list]</option>
            {lists_options}
          </Input>
          {cards_from_file &&
            <span>
              <Button className='ml-1' color='primary' onClick={add_cards}>Add cards</Button>
              <Button className='ml-1' onClick={modal_toggle}>Show new cards</Button>
            </span>
          }
        </div>
      }
    </div>
  )
}

export default Import