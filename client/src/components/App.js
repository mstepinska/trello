import React, { Component } from 'react'
import { Router} from 'react-router-dom'
import {Container, Navbar, NavbarBrand} from 'reactstrap'

import Routes from './Routes.js'

import './App.css'

class App extends Component {
  
  render() {
    return (
      <div className='h-100'>
        <Container fluid className='px-0'>
          <Navbar color="light" light expand="md">
            <NavbarBrand href='/'>Aistemos Trello</NavbarBrand>
          </Navbar>
        </Container>
        <Container fluid className='h-100 p-0 m-0'>
          <Router history={this.props.history}>
            <Routes/>
          </Router>
        </Container>
      </div>
    )
  }
}

export default App
