import React, { useEffect, useState } from 'react'
import { Input, Button } from 'reactstrap'

import { fetch_board_info, add_label_to_cards_on_list } from '../utils/boards_and_cards.js'

const Update = ({selected_board}) => {
  const [lists, setLists] = useState(null)
  const [labels, setLabels] = useState(null)
  const [list, setList] = useState(null)
  const [label, setLabel] = useState(null)


  useEffect(() => {
    fetch_board_info(selected_board.trello_id)
      .then(board_info => {
        const {lists, labels} = board_info

        const active_labels = (labels || []).filter(label => label.name !== '')

        setLists(lists)
        setLabels(active_labels)
      })
  }, [selected_board])


  function list_option_change_handler(e) {
    setList(e.target.value)
  }

  function label_option_change_handler(e) {
    setLabel(e.target.value)
  }

  function get_lists_options() {
    if (!lists) return null

    return lists.map((list, i) => (<option key={i} value={list.id}>{list.name}</option>))
  }

  function get_labels_options() {
    if (!labels) return null

    return labels.map((label, i) => (<option key={i} value={label.id}>{label.name}</option>))
  }

  function update_with_label() {
    if (!label || !list) return

    add_label_to_cards_on_list(list, label)
      .then(response => {
      })
  }

  const lists_options = get_lists_options()
  const labels_options = get_labels_options()

  return (
    <div>
      <h1>Update cards</h1>

      {lists_options &&
        <div className='mt-3'>
          <Input type='select' selected={list} onChange={list_option_change_handler}>
            <option>[select list]</option>
            {lists_options}
          </Input>
        </div>
      }

      {labels_options &&
        <div className='mt-3'>
          <Input type='select' selected={label} onChange={label_option_change_handler}>
            <option>[select label]</option>
            {labels_options}
          </Input>
        </div>
      }

      <div className='mt-3'>
        <Button color='primary' onClick={update_with_label} disabled={!label || !list}>Add selected label to the list</Button>
      </div>


    </div>
  )
}

export default Update