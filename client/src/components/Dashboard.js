import React,  { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import qs from 'query-string'
import cn from 'classnames'
import { Button, DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from 'reactstrap'

import ViewSelector from './ViewSelector.js'
import {board_export, get_available_boards} from '../utils/boards_and_cards.js'
import { get_as_map } from '../utils/utils.js'

import './Dashboard.css'

const Dashboard = (props) => {
  const query_params = qs.parse(props.location.search)

  const [boards, setBoards] = useState(null)
  const [selected_board, setSelectedBoard] = useState(null)
  const [selected_action, setSelectedAction] = useState(decodeURIComponent(query_params.todo || ''))

  useEffect(() => {
    get_available_boards()
      .then(boards => {
        const boards_by_id = get_as_map(boards, 'id')

        const query_params = qs.parse(props.location.search)
        const selected_board_id = decodeURIComponent(query_params.board || '')

        const selected_board = boards_by_id[selected_board_id] || null

        setBoards(boards)
        setSelectedBoard(selected_board)
      })
  }, [])

  function select_action(action) {
    const { location, history } = props

    const path = location.pathname
    const new_query = {...qs.parse(location.search), todo: encodeURIComponent(action)}
    history.replace({pathname: path, search: `?${qs.stringify(new_query)}`})

    setSelectedAction(action)
  }

  function select_board(board) {
    const { location, history } = props

    const path = location.pathname
    const new_query = {...qs.parse(location.search), board: encodeURIComponent(board.id)}
    history.replace({pathname: path, search: `?${qs.stringify(new_query)}`})

    setSelectedBoard(board)
  }

  function backup_all() {
    select_action('backup')
    board_export('all')
      .then(() => {

      })
  }

  return (
    <div className='d-flex w-100 h-100'>

      <div className='w-25 h-100 p-3'  style={{'backgroundColor': 'lightsalmon'}}>
        <h5>Select board</h5>
        <div className='py-2'>
        <UncontrolledDropdown>
          <DropdownToggle caret outline className='w-100'>
            {(selected_board || {}).name || 'Boards'}
          </DropdownToggle>
          <DropdownMenu>
            {(boards || []).map((board, i) => (
              <DropdownItem key={i} onClick={() => select_board(board)}>
                {board.name}
              </DropdownItem>
            ))}
          </DropdownMenu>
        </UncontrolledDropdown>
        </div>

        <h5>... and select action</h5>


        <div>
          <div className='py-2'>
            <Button disabled={!selected_board} className={cn('w-100', {'action__selected': selected_action === 'export'} )} onClick={() => select_action('export')}>Export</Button>
          </div>
          <div className='py-2'>
            <Button disabled={!selected_board} className={cn('w-100', {'action__selected': selected_action === 'import'} )} onClick={() => select_action('import')}>Import</Button>
          </div>
          <div className='py-2'>
            <Button disabled={!selected_board} className={cn('w-100', {'action__selected': selected_action === 'update'} )} onClick={() => select_action('update')}>Update</Button>
          </div>
          <div className='py-2'>
            <Button  disabled={!selected_board} className={cn('w-100', {'action__selected': selected_action === 'backup'} )} onClick={() => select_action('backup')}>Backup / Restore</Button>
          </div>
        </div>

        <div className='mt-auto flex-grow-1'>
          <Button color='primary' outline className='w-100' onClick={() => backup_all()}>Backup all boards</Button>
        </div>
      </div>

      <div className='w-75'>
        <div className='m-3'>
          <ViewSelector
            selected_action={selected_action}
            selected_board={selected_board}
          />
        </div>
      </div>

    </div>
  )
}

export default withRouter(Dashboard)