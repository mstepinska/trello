import React, { useState } from 'react'
import { Card } from 'reactstrap'
import {fetch_card_info} from '../utils/boards_and_cards.js'

const TrelloCard = ({card}) => {
  const [details, setDetails] = useState(null)

  function get_card_details(card_id) {
    fetch_card_info(card_id)
      .then(response => {
        setDetails(response)
      })
  }

  const { list='', members=[], name, shortUrl, labels=[], id } = card

  const labels_to_display = Array.isArray(labels) ? labels.map(label => (label.name)).join(', ') : labels

  return (
    <Card className='p-1'>
      <div>
        {shortUrl &&
          <a target='_blank' rel='noopener noreferrer' href={shortUrl}>{name}</a>
        }
        {!shortUrl && name}
      </div>
      <div>List: {list}</div>
      <div>Labels: {labels_to_display}</div>
      <div>Owners: {members.join(', ')}</div>

      <div onClick={() => get_card_details(id)}>Show details</div>

      {details &&
        <div>
          {JSON.stringify(details)}
        </div>
      }

    </Card>
  )

}

export default TrelloCard