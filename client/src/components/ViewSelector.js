import React from 'react'

import Export from './Export.js'
import Import from './Import.js'
import Update from './Update.js'
import Backup from './Backup.js'

const ViewSelector = ({ selected_action, selected_board } ) => {
  if (! (selected_action && selected_board)) return (
    <div>Select board and action</div>
  )

  return (
    <div>
      {selected_action === 'export' && <Export selected_board={selected_board}/>}
      {selected_action === 'import' && <Import selected_board={selected_board}/>}
      {selected_action === 'update' && <Update selected_board={selected_board}/>}
      {selected_action === 'backup' && <Backup selected_board={selected_board}/>}
    </div>
  )
}

export default ViewSelector