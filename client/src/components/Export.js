import React, { useEffect, useState } from 'react'
import saveAs from 'file-saver'

import {
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody
} from 'reactstrap'
import {fetch_board_cards} from '../utils/boards_and_cards.js'
import TrelloCard from './TrelloCard.js'
import { get_csv_string } from '../utils/csv_utils.js'

const Export = ({selected_board}) => {
  const [trello_board_data, setTrelloBoardData] = useState(null)
  const [modal_open, setModalOpen] = useState(false)
  const [list_option, setListOption] = useState('all')

  useEffect(() => {
    fetch_board_cards(selected_board.trello_id)
      .then(response => {
        setTrelloBoardData(response)
      })
  }, [selected_board])

  function modal_toggle() {
    setModalOpen(!modal_open)
  }

  function filter_cards_by_list(cards, list) {
    if (list === 'all') return cards

    return cards.filter(card => (card.idList === list))
  }

  function export_data_as_csv() {
    if (!trello_board_data) return null

    const cards = filter_cards_by_list(trello_board_data.cards, list_option)

    const data_rows = cards.map(card => {
      return [card.name, card.list, card.labels.map(label => (label.name)).join('|')]
    })
    const rows = [
      ['Name', 'List', 'Labels'],
      ...data_rows
    ]
    const csv_string = get_csv_string(rows)
    const blob = new Blob([csv_string], {type: 'application/json;charset=utf-8'})
    saveAs(blob, 'cards.csv')
  }

  function cards() {
    if (!trello_board_data) return null
    return trello_board_data.cards.map((card,i) => (<div key={i} className='my-1 mr-1' style={{'maxWidth': '100%'}}><TrelloCard card={card} /></div>))
  }

  function lists_options() {
    if (!trello_board_data) return null

    return trello_board_data.lists.map((list, i) => (<option key={i} value={list.id}>{list.name}</option>))
  }

  function list_option_change_handler(e) {
    setListOption(e.target.value)
  }

  return (
    <div>
      <Modal isOpen={modal_open} toggle={modal_toggle} size='lg'>
        <ModalHeader>All board cards</ModalHeader>
        <ModalBody>
          <div className='d-flex flex-wrap'>
            {cards()}
          </div>
        </ModalBody>
      </Modal>

      <h1>Export cards</h1>

      <div className='d-flex'>
        <Input type='select' onChange={list_option_change_handler} selected={list_option}>
          <option value='all'>All</option>
          {lists_options()}
        </Input>
        <Button className='ml-1' color='primary' onClick={export_data_as_csv}>Export to csv</Button>
        <Button className='ml-1' disabled={trello_board_data === null} onClick={modal_toggle}>Show cards</Button>
      </div>
    </div>
  )
}

export default Export