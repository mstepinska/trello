FROM node:10.15.0

# Set a working directory
WORKDIR /usr/src/app

COPY ./server ./server
COPY ./client ./client

RUN npm install -g yarn
WORKDIR /usr/src/app/client
RUN yarn install
WORKDIR /usr/src/app/server
RUN yarn install

WORKDIR /usr/src/app

RUN echo "building client" \
    && cd client \
    && yarn build \
    && echo "copying client build to server public" \
    && cd ../server \
    && cp -R ../client/build public

WORKDIR /usr/src/app/server

CMD node server.js